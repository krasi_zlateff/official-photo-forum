(function ($) {
    $(document).ready(function () {

        // Custom checkbox
        $('.custom-checkbox').off('click').on('click', function (e) {
            e.preventDefault();

            if ($(this).hasClass('checked')) {
                $(this).removeClass('checked');
            } else {
                $(this).addClass('checked');
            }
        });

        // Custom toggle checkbox
        $('.custom-switch-checkbox').bootstrapToggle({
            on: '<i class="fa fa-check"></i>',
            off: '<i class="fa fa-times"></i>',
            width: 70,
            height: 32
        });

        // Home Page - Smooth scroll to all photographers
        $('.home-page .main-header .arrow-down-wrapper').off('click').on('click', function (e) {
            e.preventDefault();

            $('html, body').stop().animate({
                scrollTop: $('.editor-photos-choice').offset().top
            }, 800);
        });

        // https://silviomoreto.github.io/bootstrap-select/options/
        $('select').selectpicker({
            size: 4,
            tickIcon: 'fa fa-check-square-o',
            showTick: true,
            showIcon: true
        });

        // Photographer Gallery - FlexSlider
        $('.item-photo-carousel').flexslider({
            animation: "slide",
            controlNav: false,
            animationLoop: false,
            slideshow: false,
            itemWidth: 60,
            itemMargin: 3,
            asNavFor: '.category-photo-slider'
        });

        // Photographer Gallery - FlexSlider carousel
        $('.item-photo-slider').flexslider({
            animation: "slide",
            controlNav: false,
            animationLoop: false,
            slideshow: false,
            sync: ".category-photo-carousel"
        });

        // Photographer Gallery - FlexSlider
        $('.category-photo-carousel').flexslider({
            animation: "slide",
            controlNav: false,
            animationLoop: false,
            slideshow: false,
            itemWidth: 60,
            itemMargin: 3,
            asNavFor: '.category-photo-slider'
        });

        // Photographer Gallery - FlexSlider carousel
        $('.category-photo-slider').flexslider({
            animation: "slide",
            controlNav: false,
            animationLoop: false,
            slideshow: false,
            sync: ".category-photo-carousel"
        });

        // Photographers Page - Price Range
        $("#bazar-items-price-ranger").slider({
            tooltip: 'always',
            tooltip_split: true
        });

        $("#resolution-mp-ranger").slider({
            tooltip: 'always',
            tooltip_split: true
        });

        // Profile page, profile tools buttons
        var profileToolsWrapper, profileActionLinks, linksWrapper, otherLinksWrapper;
        profileActionLinks = $('.profile-details .profile-tool.action-links');
        $(profileActionLinks).off('click').on('click', function (e) {
            e.preventDefault();

            profileToolsWrapper = $(this).closest('.profile-tools-wrapper');
            linksWrapper = $(this).find('.links-wrapper');
            otherLinksWrapper = $(profileToolsWrapper).find('.links-wrapper').not($(linksWrapper));

            if ($(linksWrapper).hasClass('active')) {
                $(linksWrapper).removeClass('active');
            } else {
                $(linksWrapper).addClass('active');
                $(otherLinksWrapper).removeClass('active');
            }
        });

        // Top header notifications on click
        var userAccountTool = $('.user-account-tools-wrapper .user-account-tool');
        var userNotificationsListWrapper = $(userAccountTool).find('.user-notifications-list-wrapper');

        // if the target of the click isn't the userNotificationsListWrapper nor a descendant of the userNotificationsListWrapper
        $(document).mouseup(function (e) {
            if (!userNotificationsListWrapper.is(e.target) && userNotificationsListWrapper.has(e.target).length === 0) {
                userNotificationsListWrapper.hide();
            }
        });

        $(userAccountTool).off('click').on('click', function (e) {
            e.preventDefault();
            var thisUserNotificationsListWrapper, otherUserNotificationsListWrapper;

            thisUserNotificationsListWrapper = $(this).find($(userNotificationsListWrapper));
            otherUserNotificationsListWrapper = $(userNotificationsListWrapper).not($(thisUserNotificationsListWrapper));

            if ($(thisUserNotificationsListWrapper).hasClass('active')) {
                $(thisUserNotificationsListWrapper).removeClass('active');
            } else {
                $(thisUserNotificationsListWrapper).addClass('active').show();
                $(otherUserNotificationsListWrapper).removeClass('active').hide();
            }
        });

        // Forum Show/Hide editor's of the month bio
        var readFullBio, editorMainDetails, editorBio;

        readFullBio = $('.forum .editor-of-the-month .main-details .read-more');

        $(readFullBio).off('click').on('click', function (e) {
            e.preventDefault();
            if ($(this).hasClass('active')) {
                $(this).removeClass('active').text('повече');
            } else {
                $(this).addClass('active').text('по-малко');
            }

            editorMainDetails = $(this).closest('.main-details');
            editorBio = $(editorMainDetails).find('.editor-bio');

            if ($(editorBio).hasClass('active')) {
                $(editorBio).removeClass('active').stop().animate({
                    height: 35
                }, 300)
            } else {
                $(editorBio).addClass('active').stop().animate({
                    height: $(editorBio).get(0).scrollHeight
                }, 300, function () {
                    $(this).height('auto');
                })
            }
        });

        // forum open main search box
        var openMainSearch, closeMainSearch, mainSearch;

        openMainSearch = $('.open-main-search');
        mainSearch = $('.advanced-search-wrapper');
        closeMainSearch = $(mainSearch).find('.close-search');

        $(openMainSearch).off('click').on('click', function (e) {
            e.preventDefault();

            if ($(mainSearch).hasClass('active')) {
                $(mainSearch).slideUp(800);
                $(mainSearch).removeClass('active')
            } else {
                $(mainSearch).slideDown(800);
                $(mainSearch).addClass('active')
            }
        });

        $(closeMainSearch).off('click').on('click', function (e) {
            e.preventDefault();

            if ($(mainSearch).hasClass('active')) {
                $(mainSearch).slideUp(800);
                $(mainSearch).removeClass('active')
            } else {
                $(mainSearch).slideDown(800);
                $(mainSearch).addClass('active')
            }
        });

        // Upload Photo Popup
        var uploadPhotoPopup = $('#modalUploadPhoto');

        var uploadedPhotoWrapperPopup = $(uploadPhotoPopup).find('.uploaded-photo-wrapper');
        var btnRemoveUploadedPhotoPopup = $(uploadedPhotoWrapperPopup).find('.delete-photo');

        $(btnRemoveUploadedPhotoPopup).off('click').on('click', function () {
            $(this).closest('.step-2').addClass('hidden');
            $(uploadPhotoPopup).find('.step-1').removeClass('hidden');
        });

        var seriesOfPhotos = $(uploadPhotoPopup).find('.series-of-photos');
        var sopPhotoSeriesWrapper = $(seriesOfPhotos).find('.photo-series-wrapper');
        var sopPhotoSeriesUploadedPhotoWrapper = $(sopPhotoSeriesWrapper).find('.uploaded-photo-wrapper');
        var sopRemovePhoto = $(sopPhotoSeriesUploadedPhotoWrapper).find('.delete-photo');
        var sopSelectNewPhoto = $(sopPhotoSeriesWrapper).find('.uploaded-photo-wrapper.select-new-photo');


        // remove uploaded series photo
        $(sopRemovePhoto).off('click').on('click', function () {

            $(this).closest(sopPhotoSeriesUploadedPhotoWrapper).remove();

            $(sopSelectNewPhoto).last().clone().appendTo($(sopPhotoSeriesWrapper));
        });

        // Suggest model
        $('.form-group .form-group-header a').off('click').on('click', function (e) {
            e.preventDefault();

            var thisFormGroup = $(this).closest('.form-group');
            // var filterSuggest = $(thisFormGroup).find('.form-group-body .filter-option.suggest .bootstrap-select');
            // var filterSuggestInput = $(thisFormGroup).find('.form-group-body .filter-option.suggest .suggest.form-control');

            if ($(thisFormGroup).hasClass('brand')) {
                if ($(thisFormGroup).hasClass('input-field')) {
                    $(thisFormGroup).removeClass('input-field').addClass('select-field');
                    $(this).text('Предложи марка');
                } else {
                    $(thisFormGroup).removeClass('select-field').addClass('input-field');
                    $(this).text('Избери марка');
                }
            } else if ($(thisFormGroup).hasClass('model')) {
                if ($(thisFormGroup).hasClass('select-field')) {
                    $(thisFormGroup).removeClass('select-field').addClass('input-field');
                    $(this).text('Избери модел');
                } else {
                    $(thisFormGroup).removeClass('input-field').addClass('select-field');
                    $(this).text('Предложи модел');
                }
            }
        });

        // =================================
        // Adjust content font size
        // =================================
        var contentFontTool = $('.content-font-tool');
        var decreaseFont = $(contentFontTool).find('.btn-font-size.decrease');
        var enlargeFont = $(contentFontTool).find('.btn-font-size.enlarge');
        var defaultFont = $(contentFontTool).find('.btn-font-size.default');

        // Decrease content font size
        $(decreaseFont).off('click').on('click', function (e) {
            e.preventDefault();
            var contentFontSize = parseInt($('p').css('font-size'));
            if (contentFontSize > 8) {
                contentFontSize = contentFontSize - 1 + "px";
                $('p').css({'font-size': contentFontSize});
            }
        });

        // Enlarge content font size
        $(enlargeFont).off('click').on('click', function (e) {
            e.preventDefault();
            var contentFontSize = parseInt($('p').css('font-size'));
            if (contentFontSize < 28) {
                contentFontSize = contentFontSize + 1 + "px";
                $('p').css({'font-size': contentFontSize});
            }
        });

        // Default content font size
        $(defaultFont).off('click').on('click', function (e) {
            e.preventDefault();
            $('p').css('font-size', '14px')
        });

        // Bazar list/grid view
        var bazarProducts = $('.bazar-products');
        var bazarProductsItem = $(bazarProducts).find('.item-wrapper');
        var bazarProductsItemImage = $(bazarProductsItem).find('.item-image');
        var bazarProductsItemCaption = $(bazarProductsItem).find('.item-caption');
        var bazarProductsItemPrice = $(bazarProductsItem).find('.item-caption-price');
        var bazarProductsItemButtons = $(bazarProductsItem).find('.item-caption-buttons');

        // Bazar products List View
        $('#change-list-view').off('click').on('click', function (e) {
            e.preventDefault();

            $(bazarProducts).removeClass('grid-view');
            $(bazarProducts).addClass('list-view');

            $(bazarProductsItem).removeClass('col-xs-4 col-sm-4 col-md-4 col-lg-4 grid-view-item');
            $(bazarProductsItem).addClass('col-xs-12 col-sm-12 col-md-12 col-lg-12 list-view-item');

            $(bazarProductsItemImage).removeClass('col-xs-12 col-sm-12 col-md-12 col-lg-12 grid-view-image');
            $(bazarProductsItemImage).addClass('col-xs-4 col-sm-4 col-md-4 col-lg-4 list-view-image');

            $(bazarProductsItemCaption).removeClass('col-xs-12 col-sm-12 col-md-12 col-lg-12 grid-view-caption');
            $(bazarProductsItemCaption).addClass('col-xs-8 col-sm-8 col-md-8 col-lg-8 list-view-caption');

            $(bazarProductsItemPrice).removeClass('col-xs-12 col-sm-12 col-md-12 col-lg-12 grid-view-price');
            $(bazarProductsItemPrice).addClass('col-xs-6 col-sm-6 col-md-5 col-lg-5 list-view-price');

            $(bazarProductsItemButtons).removeClass('col-xs-12 col-sm-12 col-md-12 col-lg-12 grid-view-buttons');
            $(bazarProductsItemButtons).addClass('col-xs-6 col-sm-6 col-md-7 col-lg-7 list-view-buttons');
        });

        // Bazar products Grid View
        $('#change-grid-view').off('click').on('click', function (e) {
            e.preventDefault();

            $(bazarProducts).removeClass('list-view');
            $(bazarProducts).addClass('grid-view');

            $(bazarProductsItem).removeClass('col-xs-12 col-sm-12 col-md-12 col-lg-12 list-view-item');
            $(bazarProductsItem).addClass('col-xs-4 col-sm-4 col-md-4 col-lg-4 grid-view-item');

            $(bazarProductsItemImage).removeClass('col-xs-4 col-sm-4 col-md-4 col-lg-4 list-view-image');
            $(bazarProductsItemImage).addClass('col-xs-12 col-sm-12 col-md-12 col-lg-12 grid-view-image');

            $(bazarProductsItemCaption).removeClass('col-xs-8 col-sm-8 col-md-8 col-lg-8 list-view-caption');
            $(bazarProductsItemCaption).addClass('col-xs-12 col-sm-12 col-md-12 col-lg-12 grid-view-caption');

            $(bazarProductsItemPrice).removeClass('col-xs-6 col-sm-6 col-md-5 col-lg-5 list-view-price');
            $(bazarProductsItemPrice).addClass('col-xs-12 col-sm-12 col-md-12 col-lg-12 grid-view-price');

            $(bazarProductsItemButtons).removeClass('col-xs-6 col-sm-6 col-md-7 col-lg-7 list-view-buttons');
            $(bazarProductsItemButtons).addClass('col-xs-12 col-sm-12 col-md-12 col-lg-12 grid-view-buttons');
        });

        var bazar_add_seriesOfPhotos = $('.bazar-ad.series-of-photos');
        var bazar_add_sop_Wrapper = $(bazar_add_seriesOfPhotos).find('.photo-series-wrapper');
        var bazar_add_sop_UploadedPhotoWrapper = $(bazar_add_sop_Wrapper).find('.uploaded-photo-wrapper');
        var bazar_add_sop_RemovePhoto = $(bazar_add_sop_UploadedPhotoWrapper).find('.delete-photo');
        var bazar_add_sop_SelectNewPhoto = $(bazar_add_sop_Wrapper).find('.uploaded-photo-wrapper.select-new-photo');

        // remove uploaded series photo
        $(bazar_add_sop_RemovePhoto).off('click').on('click', function () {
            $(this).closest(bazar_add_sop_UploadedPhotoWrapper).remove();
            $(bazar_add_sop_SelectNewPhoto).last().clone().appendTo($(bazar_add_sop_Wrapper));
        });

        var modal_bazar_add_seriesOfPhotos = $('.modal-bazar-ad.series-of-photos');
        var modal_bazar_add_sop_Wrapper = $(modal_bazar_add_seriesOfPhotos).find('.photo-series-wrapper');
        var modal_bazar_add_sop_UploadedPhotoWrapper = $(modal_bazar_add_sop_Wrapper).find('.uploaded-photo-wrapper');
        var modal_bazar_add_sop_RemovePhoto = $(modal_bazar_add_sop_UploadedPhotoWrapper).find('.delete-photo');
        var modal_bazar_add_sop_SelectNewPhoto = $(modal_bazar_add_sop_Wrapper).find('.uploaded-photo-wrapper.select-new-photo');

        // remove uploaded series photo
        $(modal_bazar_add_sop_RemovePhoto).off('click').on('click', function () {
            $(this).closest(modal_bazar_add_sop_UploadedPhotoWrapper).remove();
            $(modal_bazar_add_sop_SelectNewPhoto).last().clone().appendTo($(modal_bazar_add_sop_Wrapper));
        });

        // Show toast messages.
        // https://github.com/CodeSeven/toastr
        // http://codeseven.github.io/toastr/demo.html

        // Show success toastr
        $('.btn-success').off('click').on('click', function () {
            toastr.success('Запази');
        });

        // Show info toastr
        $('.btn-info').off('click').on('click', function () {
            toastr.info('Инфо');
        });

        // Show warning toastr
        $('.btn-warning').off('click').on('click', function () {
            toastr.warning('Внимание');
        });

        $('.btn-danger').off('click').on('click', function () {
            toastr.error('Изтрий');
        });

        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": true,
            "positionClass": "toast-top-right",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };


        // var el = document.querySelectorAll('.profile-header-image-wrapper');
        // var vanilla = new Croppie(el, {
        //     viewport: {width: 960, height: 150},
        //     enableZoom: true,
        //     mouseWheelZoom: true,
        //     showZoomer: false
        // });
        // vanilla.bind({
        //     url: '../images/backgrounds/profile_header_background.jpg'
        // });

        // Croppie Demo usage
        var basic = $('.profile-header-image-wrapper').croppie({
            viewport: {
                width: 960,
                height: 150
            }
        });
        basic.croppie('bind', {
            url: '../images/backgrounds/profile_header_background.jpg',
            enableZoom: true,
            mouseWheelZoom: true,
            showZoomer: false
        });

    });

    // When window object is ready or loaded
    $(window).load(function () {

        // var uploadNewCoverPhoto = $('.buttons-change-cover-photo .btn-group .dropdown-menu li');
        // var uploadNewCoverPhotoWidth = $(uploadNewCoverPhoto).outerWidth();
        // var uploadNewCoverPhotoHeight = $(uploadNewCoverPhoto).outerHeight();
        // var uploadCoverPhotoWrapper = $(uploadNewCoverPhoto).find('#upload-cover-photo');
        //
        // $(uploadCoverPhotoWrapper).css('width', uploadNewCoverPhotoWidth);
        // $(uploadCoverPhotoWrapper).css('height', uploadNewCoverPhotoHeight);

        // Profile menu
        var profileMenu = $('.profile-menu');
        var profileMenuElement = $(profileMenu).find('.menu-element');
        var profileMenuWidth = $(profileMenu).outerWidth();
        var profileMenuPosition = parseInt($(profileMenu).css("right"));
        var profileMenuClose = $(profileMenu).find('.close-menu');
        var profileMenuCloseWidth = $(profileMenuClose).outerWidth();
        var profileName = $('header.top-header .account-wrapper .user-account.logged .top-menu .menu-item .menu-element.profile-name');
        var profilePhotoImage = $(profileName).find('.profile-photo');
        var profileNameText = $(profileName).find('span.text');

        $(profileMenu).css('width', profileMenuWidth + profileMenuCloseWidth);
        $(profileMenu).css('right', -profileMenuWidth - profileMenuCloseWidth);
        $(profileMenu).css('display', 'block');

        // Open Profile menu
        $(profileName).off('click').on('click', function (e) {
            e.preventDefault();
            if (parseInt($(topMobileMenuWrapper).css('left')) === 0) {
                $(topMobileMenuWrapper).stop().animate({
                    left: -topMobileMenuWrapperWidth
                }, 300);
            }
            if (profileMenuPosition < 0) {
                $(profileMenu).stop().animate({
                    right: 0
                }, 300);
            }
        });

        // Close Profile menu
        $(profileMenuClose).off('click').on('click', function (e) {
            e.preventDefault();
            $(profileMenu).stop().animate({
                right: -profileMenuWidth - profileMenuCloseWidth
            }, 300);
        });

        // Profile menu element
        $(profileMenuElement).off('click').on('click', function (e) {
            $(profileMenu).stop().animate({
                right: -profileMenuWidth - profileMenuCloseWidth
            }, 300);
        });

        // Hide profile menu on outside clicking
        $(document).on('click', function (e) {
            if ((!profilePhotoImage.is(e.target) && profilePhotoImage.has(e.target).length === 0) &&
                (!profileNameText.is(e.target) && profileNameText.has(e.target).length === 0)) {
                $(profileMenu).stop().animate({
                    right: -profileMenuWidth - profileMenuCloseWidth
                }, 300);
            }
        });

        // Mobile menu
        var topMobileMenuWrapper = $('.top-header .mobile-menu-wrapper');
        var topMobileMenu = $(topMobileMenuWrapper).find('.top-menu');
        var topMobileMenuhasSubmenu = $(topMobileMenu).find('.menu-item.has-submenu');
        var topMobileMenuhasSubmenuMenuElement = $(topMobileMenuhasSubmenu).find('.menu-element:first');
        var topMobileMenuIcon = $('.top-header .mobile-menu .mobile-menu-hamburger');
        var topMobileMenuWrapperWidth = parseInt($(topMobileMenuWrapper).css('width'));
        var topMobileMenuWrapperPosition = parseInt($(topMobileMenuWrapper).css("left"));
        var topMobileMenuCloseButton = $(topMobileMenuWrapper).find('.mobile-menu-nav .menu-item .close-menu');
        var topMobileMenuBackButton = $(topMobileMenuWrapper).find('.top-menu .menu-item.has-submenu .submenu .back-menu-icon');

        // Open top mobile menu
        $(topMobileMenuIcon).off('click').on('click', function (e) {
            e.preventDefault();
            if (parseInt($(profileMenu).css('right')) === 0) {
                $(profileMenu).stop().animate({
                    right: -profileMenuWidth - profileMenuCloseWidth
                }, 300);
            }
            if (topMobileMenuWrapperPosition < 0) {
                $(topMobileMenuWrapper).stop().animate({
                    left: 0
                }, 300);
            }
        });

        // Close top mobile menu
        $(topMobileMenuCloseButton).off('click').on('click', function (e) {
            e.preventDefault();
            $(topMobileMenuWrapper).stop().animate({
                left: -topMobileMenuWrapperWidth
            }, 300);
        });

        // Open top mobile submenu
        $(topMobileMenuhasSubmenuMenuElement).off('click').on('click', function (e) {
            e.preventDefault();
            $(topMobileMenuBackButton).stop().animate({
                opacity: 1
            }, 300);
            var topMobileMenuSubMenu = $(this).closest($(topMobileMenuhasSubmenu)).find('.submenu:first').addClass('active');
            var topMobileMenuSubMenuPosition = parseInt($(topMobileMenuSubMenu).css('left'));
            if (topMobileMenuSubMenuPosition < 0) {
                $(topMobileMenuSubMenu).stop().animate({
                    left: 0
                }, 300);
            }
        });

        // Close top mobile submenu
        $(topMobileMenuBackButton).off('click').on('click', function (e) {
            e.preventDefault();
            var topMobileMenuSubMenu = $(topMobileMenuhasSubmenu).find('.submenu');

            if ($(topMobileMenuSubMenu).hasClass('active')) {
                var topMobileMenuSubMenuWidth = parseInt($(topMobileMenuSubMenu).css('width'));
                $(topMobileMenuSubMenu).stop().animate({
                    left: -topMobileMenuSubMenuWidth
                });
                $(topMobileMenuSubMenu).removeClass('active');
                $(this).stop().animate({
                    opacity: 0
                }, 300);
            }
        });

        var slideList = $('.main-photo-viewer .category-photo-slider .slides');
        var slideListPhotoImage = $(slideList).find('li.slide-item .slide-photo img');

        var loadPhotoImage = function () {
            $(slideListPhotoImage).each(function () {
                // Hack!!! attach current time behind the image path in order to prevent caching
                $(this).attr('src', $(this).attr('src') + '?' + new Date().getTime())
            }).on('load', function () {
                var slideListPhotoWidth = $(this).width();
                var slideListPhotoHeight = $(this).height();
                var slideListPhotoImageSrc = $(this).attr('src');

                if (slideListPhotoWidth > slideListPhotoHeight) {
                    var slideListPhoto = $(this).closest('.slide-photo');
                    slideListPhoto.css('background-image', 'url("' + slideListPhotoImageSrc + '")');
                    // $(this).hide();
                    $(this).remove()
                }
            });
        };
        // Hack!!! set timeout in order to let the rendering threads catch up.
        setTimeout(loadPhotoImage, 0);
    });

    $(window).scroll(function () {
        var bazarVerticalAd = $('.bazar-vertical-side-ad');
        var windowHeight = $(window).scrollTop() + $(window).height();
        var footerHeight = $('.main-footer').outerHeight(true);

        function getDocHeight() {
            var D = document;
            return Math.max(
                D.body.scrollHeight, D.documentElement.scrollHeight,
                D.body.offsetHeight, D.documentElement.offsetHeight,
                D.body.clientHeight, D.documentElement.clientHeight
            );
        }

        if (windowHeight > getDocHeight() - footerHeight) {
            $(bazarVerticalAd).css({
                'bottom': footerHeight + 80,
                'position': 'absolute'
            })
        } else {
            $(bazarVerticalAd).css({
                'bottom': 80,
                'position': 'fixed'
            })
        }
    });
})(jQuery);
