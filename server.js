var express = require('express');
var app = express();

// set the port of our application
// process.env.PORT lets the port be set by Heroku
var port = process.env.PORT || 8080;

// set the view engine to ejs
app.set('view engine', 'ejs');

// make express look in the public directory for assets (css/js/img)
app.use(express.static(__dirname + '/public'));
app.use(express.static(__dirname + '/javascripts'));
app.use(express.static(__dirname + '/bower_libraries'));

// set the home page route
app.get('/', function (req, res) {
  // ejs render automatically looks in the views folder
  res.render('index');
});

// set the ratinh page route
app.get('/rating', function (req, res) {
  // ejs render automatically looks in the views folder
  res.render('rating');
});

// set the user's all messages page route
app.get('/user-messages', function (req, res) {
  // ejs render automatically looks in the views folder
  res.render('user_messages');
});

// set the user's single message page route
app.get('/user-message', function (req, res) {
  // ejs render automatically looks in the views folder
  res.render('user_message');
});

// set the registration page route
app.get('/registration', function (req, res) {
  // ejs render automatically looks in the views folder
  res.render('registration/registration');
});

// set the registration finish page route
app.get('/registration-finish', function (req, res) {
  // ejs render automatically looks in the views folder
  res.render('registration/registration_finished');
});

// set the forgotten password page route
app.get('/forgotten-password', function (req, res) {
  // ejs render automatically looks in the views folder
  res.render('forgotten_password');
});

// set the profile page route
app.get('/profile', function (req, res) {
  // ejs render automatically looks in the views folder
  res.render('profile/profile');
});

// set the profile logged page route
app.get('/profile-logged', function (req, res) {
  // ejs render automatically looks in the views folder
  res.render('profile/profile_logged');
});

// set the profile photo album page route
app.get('/profile-photo-album', function (req, res) {
    // ejs render automatically looks in the views folder
    res.render('profile/photo_album');
});

// set the profile edit page route
app.get('/profile-edit', function (req, res) {
  // ejs render automatically looks in the views folder
  res.render('profile/profile_edit');
});

// set the profile page route
app.get('/notifications', function (req, res) {
  // ejs render automatically looks in the views folder
  res.render('notifications');
});

// set the gallery page route
app.get('/gallery', function (req, res) {
  // ejs render automatically looks in the views folder
  res.render('gallery');
});

// set the last comments page route
app.get('/last-comments', function (req, res) {
  // ejs render automatically looks in the views folder
  res.render('last_comments');
});

// set the forum all topics page route
app.get('/forum-all-topics', function (req, res) {
  // ejs render automatically looks in the views folder
  res.render('forum/topics_all');
});

// set the forum new topic page route
app.get('/forum-new-topic', function (req, res) {
  // ejs render automatically looks in the views folder
  res.render('forum/topic_new');
});

// set the forum topic page route
app.get('/forum-topic', function (req, res) {
  // ejs render automatically looks in the views folder
  res.render('forum/topic');
});

// set the news-story page route
app.get('/news-story', function (req, res) {
  // ejs render automatically looks in the views folder
  res.render('news/news_story');
});

// set the news-read page route
app.get('/news-read', function (req, res) {
  // ejs render automatically looks in the views folder
  res.render('news/news_read');
});

// set the event-read page route
app.get('/event-read', function (req, res) {
  // ejs render automatically looks in the views folder
  res.render('event/event_read');
});

// set the bazar all ads page route
app.get('/bazar', function (req, res) {
  // ejs render automatically looks in the views folder
  res.render('bazar/ads_all');
});

// set the bazar view ad page route
app.get('/bazar-view-ad', function (req, res) {
  // ejs render automatically looks in the views folder
  res.render('bazar/ad_view');
});

// set the bazar add ad page route
app.get('/bazar-add-ad', function (req, res) {
  // ejs render automatically looks in the views folder
  res.render('bazar/ad_add');
});

// set the editor view page route
app.get('/editor-view', function (req, res) {
  // ejs render automatically looks in the views folder
  res.render('editors/editor_view');
});

// set the editors view page route
app.get('/editors', function (req, res) {
  // ejs render automatically looks in the views folder
  res.render('editors/editors_all');
});

// set the useful comments page route
app.get('/useful-comments', function (req, res) {
  // ejs render automatically looks in the views folder
  res.render('polezni_komentari');
});

// set the rating history page route
app.get('/rating-history', function (req, res) {
  // ejs render automatically looks in the views folder
  res.render('rating_history/index');
});

// set the rating history month page route
app.get('/rating-history-month', function (req, res) {
    // ejs render automatically looks in the views folder
    res.render('rating_history/month');
});

// set the forum fest page route
app.get('/forum-fest', function (req, res) {
    // ejs render automatically looks in the views folder
    res.render('forum_fest');
});

// set the photopis page route
app.get('/photopis', function (req, res) {
    // ejs render automatically looks in the views folder
    res.render('photopis/index');
});

// set the photopis view page route
app.get('/photopis-view', function (req, res) {
    // ejs render automatically looks in the views folder
    res.render('photopis/view');
});

// set the photopis edit page route
app.get('/photopis-edit', function (req, res) {
    // ejs render automatically looks in the views folder
    res.render('photopis/edit');
});

// set the camera item view page route
app.get('/camera-item-view', function (req, res) {
    // ejs render automatically looks in the views folder
    res.render('camera_item_view');
});

// set the photo page route
app.get('/photo', function (req, res) {
    // ejs render automatically looks in the views folder
    res.render('photo/photo');
});

// set the photo page route
app.get('/photo-edit', function (req, res) {
    // ejs render automatically looks in the views folder
    res.render('photo/photo_edit');
});

// set the photo not found page route
app.get('/photo-not-found', function (req, res) {
    // ejs render automatically looks in the views folder
    res.render('photo/photo_not_found');
});

// set the licenses page route
app.get('/licenses', function (req, res) {
    // ejs render automatically looks in the views folder
    res.render('licenses');
});

// set the search results page route
app.get('/search-results', function (req, res) {
    // ejs render automatically looks in the views folder
    res.render('search_results');
});

// set the store page route
app.get('/store', function (req, res) {
    // ejs render automatically looks in the views folder
    res.render('store/grid_view');
});

// set the store item page route
app.get('/store-item', function (req, res) {
    // ejs render automatically looks in the views folder
    res.render('store/item');
});

// set the store store cart 1 page route
app.get('/store-cart-1', function (req, res) {
    // ejs render automatically looks in the views folder
    res.render('store/shopping_cart/cart_1');
});

// set the store store cart 2 page route
app.get('/store-cart-2', function (req, res) {
    // ejs render automatically looks in the views folder
    res.render('store/shopping_cart/cart_2');
});

// set the store store cart 3 page route
app.get('/store-cart-3', function (req, res) {
    // ejs render automatically looks in the views folder
    res.render('store/shopping_cart/cart_3');
});

app.listen(port, function () {
  console.log('Our app is running on http://localhost:' + port);
});
